<?php

use App\Http\Controllers\Admin\InfoController;
use App\Models\Info;
use Illuminate\Support\Facades\Route;


Route::view('/', 'welcome', ['info' => Info::query()->first()])
    ->name('welcome');

Route::view('/dashboard', 'dashboard')
    ->name('admin.dashboard');

Route::middleware([])
    ->name('admin.')
    ->group(function () {
        Route::resource('infos', InfoController::class);
    });
