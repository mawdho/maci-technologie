<footer class="relative bg-gray-300 pt-8 pb-6">
  <div class="bottom-auto top-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden -mt-20"
    style="height: 80px;">
    <svg class="absolute bottom-0 overflow-hidden" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none"
      version="1.1" viewBox="0 0 2560 100" x="0" y="0">
      <polygon class="text-gray-300 fill-current" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
  <div class="container mx-auto px-4">
    <div class="flex flex-wrap">
      <div class="w-full px-4">
        <h4 class="text-3xl font-semibold">Gardons le contact !</h4>
        <h5 class="text-lg mt-0 mb-2 text-gray-700">
          Trouvez-nous sur chacune de ces plateformes, on répond à vos differentes requêtes ...
        </h5>
        <div class="flex justify-between items-center">
          <div class="mt-6">
            <a href="{{ $info->twitter }}">
              <button
                class="bg-white text-blue-400 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                type="button">
                <i class="flex fab fa-twitter"></i>
              </button>
            </a>
            <a href="{{ $info->facebook }}">
              <button
                class="bg-white text-blue-600 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                type="button">
                <i class="flex fab fa-facebook-square"></i>
              </button>
            </a>
            <a href="{{ $info->whatsapp }}">
              <button
                class="bg-white text-green-400 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                type="button">
                <i class="flex fab fa-whatsapp"></i>
              </button>
            </a>
            <a href="mailto:{{ $info->youtube }}">
              <button
                class="bg-white text-red-900 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                type="button">
                <i class="flex fab fa-youtube"></i>
              </button>
            </a>
          </div>
          <div class="text-xl px-3 py-1 bg-gray-100">
            <span class="fa fa-at"> : <strong>{{ $info->email }}</strong> </span>
          </div><div class="text-xl px-3 py-1 bg-gray-100">
            <span class="fa fa-phone-alt"> : <strong>{{ $info->telephone }}</strong> </span>
          </div>
        </div>
      </div>
    </div>
    <hr class="my-6 border-gray-400" />
    <div class="flex flex-wrap items-center md:justify-between justify-center">
      <div class="w-full md:w-4/12 px-4 mx-auto text-center">
        <div class="text-sm text-gray-600 font-semibold uppercase py-1">
          Copyright &COPY;
          {{ now()->year . ' - ' . $info->nom }} par
          <span class="text-blue-700 text-2xl">TAOB</span>
        </div>
      </div>
    </div>
  </div>
</footer>