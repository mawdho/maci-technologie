@extends('admin.layouts.app')

@section('admin.content')
<section class="bg-white mx-auto shadow-lg">
  <div class="text-center mb-4">
    <h2 class="text-xl font-semibold capitalize underline tracking-wider leading-relaxed">Modifier les infos de
      l'entreprise</h2>
  </div>
  <div class="mx-4">
    <form action="{{ route('admin.infos.update', $info) }}" method="post">
      @csrf @method('PUT')
      <div class="lg:flex justify-between items-center mb-6">
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="nom" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider"> Nom de
            l'entreprise</label>
          <input type="text" id="nom" name="nom" value="{{ old('nom') ?? $info->nom }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="slogan" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Slogan</label>
          <input type="text" id="slogan" name="slogan" value="{{ old('slogan') ?? $info->slogan }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
      </div>
      <div class="lg:flex justify-between items-center mb-6">
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="adresse"
            class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Adresse</label>
          <input type="text" id="adresse" name="adresse" value="{{ old('adresse') ?? $info->adresse }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="telephone"
            class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Telephone</label>
          <input type="text" id="telephone" name="telephone" value="{{ old('telephone') ?? $info->telephone }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
      </div>
      <div class="lg:flex justify-between items-center mb-6">
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="email" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Email</label>
          <input type="email" id="email" name="email" value="{{ old('email') ?? $info->email }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="facebook" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Lien vers la
            page Facebook</label>
          <input type="text" id="facebook" name="facebook" value="{{ old('facebook') ?? $info->facebook }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
      </div>
      <div class="lg:flex justify-between items-center mb-6">
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="twitter" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Lien vers la
            page Twitter</label>
          <input type="text" id="twitter" name="twitter" value="{{ old('twitter') ?? $info->twitter }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="whatsapp" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Lien vers la
            page Whatsapp</label>
          <input type="text" id="whatsapp" name="whatsapp" value="{{ old('whatsapp') ?? $info->whatsapp }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
      </div>
      <div class="lg:flex justify-between items-center mb-6">
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="youtube" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Lien vers la
            page Youtube</label>
          <input type="text" id="youtube" name="youtube" value="{{ old('youtube') ?? $info->youtube }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="hero_title" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Titre
            d'accueil</label>
          <input type="text" id="hero_title" name="hero_title" value="{{ old('hero_title') ?? $info->hero_title }}"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full">
        </div>
      </div>
      <div class="lg:flex justify-between items-center mb-6">
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="hero_desc" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">Message
            d'accueil</label>
          <textarea name="hero_desc" id="hero_desc" cols="30" rows="10"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full text-justify">{{ old('hero_desc') ?? $info->hero_desc }}</textarea>
        </div>
        <div class="w-full mr-4 mb-4 lg:mb-0">
          <label for="apropos" class="text-gray-600 text-lg block font-semibold capitalize tracking-wider">A Propos de
            L'entreprise</label>
          <textarea name="apropos" id="apropos" cols="30" rows="10"
            class="px-4 py-2 border border-gray-300 rounded-lg text-gray-600 text-md tracking-wider focus:outline-none w-full text-justify">{{ old('apropos') ?? $info->apropos }}</textarea>
        </div>
      </div>
      <div class="flex place-content-center pb-6">
        <button type="submit"
          class="px-4 py-2 rounded-lg w-1/2 text-white bg-indigo-700 hover:bg-indigo-500 font-bold tracking-widest text-xl">
          <i class="fa fa-check mr-2"></i>
          <span>Valider</span>
        </button>
      </div>
    </form>
  </div>
</section>
@endsection