@extends('admin.layouts.app')

@section('admin.content')

<section class="bg-white flex flex-col lg:flex-row p-2">
  <div class="bg-gray-200 shadow-lg w-full lg:w-1/3">
    <div class="">
      <img src="{{ asset('logo.jpeg') }}" class="">
    </div>
    <ul class="bg-white p-2 m-3">
      <h2 class="text-lg capitalize text-center underline text-gray-700 mb-2">
        Les réseaux sociaux
      </h2>
      <li class="border-2 border-separate border-gray-600 text-gray-600 rounded-md px-4 py-2 mb-3 break-words">
        <i class="fa fa-at"></i>
        <span>{{ $info->email }}</span>
      </li>
      <li class="border-2 border-separate border-teal-600 text-teal-600 rounded-md px-4 py-2 mb-3">
        <i class="fa fa-phone-alt"></i>
        <span>{{ $info->telephone }}</span>
      </li>
      <li class="text-sm border-2 border-separate border-blue-600 text-blue-600 rounded-md px-4 py-2 mb-3 break-words">
        <i class="fab fa-facebook"></i>
        <span>{{ $info->facebook }}</span>
      </li>
      <li
        class="text-sm border-2 border-separate border-green-600 text-green-600 rounded-md px-4 py-2 mb-3 break-words">
        <i class="fab fa-whatsapp"></i>
        <span>{{ $info->whatsapp }}</span>
      </li>
      <li class="text-sm border-2 border-separate border-cyan-600 text-cyan-600 rounded-md px-4 py-2 mb-3 break-words">
        <i class="fab fa-twitter"></i>
        <span>{{ $info->twitter }}</span>
      </li>
      <li class="text-sm border-2 border-separate border-red-600 text-red-600 rounded-md px-4 py-2 mb-3 break-words">
        <i class="fab fa-youtube"></i>
        <span class="">{{ $info->youtube }}</span>
      </li>
    </ul>
  </div>
  <div class="bg-gray-200 shadow-lg p-4 w-full lg:w-2/3">
    <div class="flex lg:flex-row flex-col-reverse justify-between">
      <div class="w-full lg:w-4/6">
        <h1
          class="text-xl text-indigo-800 text-center font-bold font-mono uppercase underline tracking-widest leading-loose mb-2">
          {{ $info->nom }}
        </h1>
        <h6 class="text-xs text-center text-gray-600 uppercase mb-5">"
          {{ $info->slogan }}
          "</h6>
        <h6 class="text-base text-center mb-3">
          {{ $info->adresse }}
        </h6>
      </div>
      <div class="hidden lg:block w-2/6">
        <a href="{{ route('admin.infos.edit', $info) }}" class="ml-4 text-xs bg-blue-700 hover:bg-blue-500 text-white px-4 py-2 w-1/2 text-center float-right">
          <i class="fa fa-edit"></i>
          <span>Editer</span>
        </a>
      </div>
    </div>
    <div class="bg-white border-2 border-gray-500 rounded-lg py-1 px-2 mb-3">
      <h2 class="text-lg font-semibold capitalize text-center underline text-gray-700 mb-2">
        A propos de l'entreprise
      </h2>
      <p class="text-base text-justify">
        {{ $info->apropos }}
      </p>
    </div>
    <div class="bg-white border-2 border-gray-500 rounded-lg py-1 px-2 ">
      <h2 class="text-lg font-semibold capitalize text-center underline text-gray-700 mb-2">
        Le héro section
      </h2>
      <div class="">
        <h4 class="text-base capitalize text-justify font font-semibold underline mb-2">
          {{ $info->hero_title }}
        </h4>
      </div>
      <div class="">
        <p class="text-base text-justify">
          {{ $info->hero_desc }}
        </p>
      </div>
    </div>
  </div>
</section>


@endsection