<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Info>
 */
class InfoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nom' => 'Maci Technologie Btp',
            'slogan' => 'Bravour - Excellence - Rconnaissance',
            'hero_title' => 'Votre histoire commence ici et c\'est avec nous.',
            'hero_desc' => 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Nulla quis lorem ut libero malesuada feugiat. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh.',
            'adresse' => 'Hafia minière, C. Dixinn - Conakry/Rép. de Guinée.',
            'email' => 'bahlesultan@gmail.com',
            'telephone' => '622 00 09 18',
            'facebook' => 'https://www.facebook.com/macitechbtp/',
            'twitter' => 'https://twitter.com/BahBohido?t=6TvZRG-H5QWFAI_qC-Oe7Q&s=09',
            'whatsapp' => 'https://wa.me/message/4RXNMY6Y2UWWL1',
            'youtube' => 'https://youtube.com/channel/UCfNXg__oeQTc7T9pqEPS8kg',
            'apropos' => 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Nulla quis lorem ut libero malesuada feugiat. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh.',
        ];
    }
}
