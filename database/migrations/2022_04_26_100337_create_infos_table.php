<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos', function (Blueprint $table) {
            $table->id();

            $table->string('nom');
            $table->string('slogan');
            $table->string('hero_title');
            $table->mediumText('hero_desc');
            $table->string('adresse');
            $table->string('email');
            $table->string('telephone');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('whatsapp');
            $table->string('youtube');
            $table->text('apropos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
};
