<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Info;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = Info::query()->first();
        return view('admin.infos.index', compact('info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.infos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Info::query()->create([
            'nom' => $request->nom,
            'slogan' => $request->slogan,
            'hero_title' => $request->hero_title,
            'hero_desc' => $request->hero_desc,
            'adresse' => $request->adresse,
            'email' => $request->email,
            'telephone' => $request->telephone,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'whatsapp' => $request->whatsapp,
            'youtube' => $request->youtube,
            'apropos' => $request->apropos,
        ]);

        // toastr()->info('L\'info a été ajoutée avec succès !!!');
        return to_route('admin.infos.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Info  $info
     * @return \Illuminate\Http\Response
     */
    public function edit(Info $info)
    {
        if ($info) {
            return view('admin.infos.edit', compact('info'));
        } else {
            // toastr()->error('L\'info selectionnée n\'existe pas !!!');
            return back();
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Info  $info
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Info $info)
    {
        $info->update([
            'nom' => $request->nom,
            'slogan' => $request->slogan,
            'hero_title' => $request->hero_title,
            'hero_desc' => $request->hero_desc,
            'adresse' => $request->adresse,
            'email' => $request->email,
            'telephone' => $request->telephone,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'whatsapp' => $request->whatsapp,
            'youtube' => $request->youtube,
            'apropos' => $request->apropos,
        ]);
        return to_route('admin.infos.index')->with('info', 'Les informations ont été modifiée avec succès !!!');
    }
}
